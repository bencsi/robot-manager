INSERT INTO `type` (`id`, `type`) VALUES
  (1, 'Android'),
  (3, 'Cyborg'),
  (2, 'Mecha');

INSERT INTO `robot` (`id`, `type_id`, `name`, `year`, `created`, `deleted`) VALUES
(1, 2, 'Hector', 1980, '2015-12-28 16:25:24', 0),
(2, 1, 'David 8', 2012, '2015-12-28 16:25:59', 0),
(3, 1, 'Bishop', 1986, '2015-12-28 16:26:22', 0);