<?php

namespace AppBundle\Controller\Rest;

use Symfony\Component\HttpFoundation\Request;

interface RestInterface
{

    public function getItem(Request $request);

    public function getAll();

    public function update(Request $request);

    public function create(Request $request);

    public function delete(Request $request);
}