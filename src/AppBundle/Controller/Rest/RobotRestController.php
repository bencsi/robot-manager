<?php

namespace AppBundle\Controller\Rest;

use AppBundle\Entity\Robot;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class RobotRestController extends AbstractRestController
{
    const MESSAGE_NAME_VALUE_MUST_BE_UNIQUE = 'Name value must be unique';
    const MESSAGE_TYPE_VALUE_IS_INVALID = 'Type value is invalid';
    const MESSAGE_PARAMETER_YEAR_MUST_BE_4_DIGIT = 'Parameter `year` must be 4-digit';

    protected function getRepo()
    {
       return $this->getDoctrine()->getRepository('AppBundle:Robot');
    }

    /**
     * @Route("/robots/{id}", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     */
    public function getItem(Request $request)
    {
        $id = $request->get('id');
        $data =  $this->getRepo()->findRobot($id);
        $result = parent::resultBuilder(parent::STATUS_FOUND, parent::STATUS_NOT_FOUND, $data);

        return parent::responseBuilder($result);
    }

    /**
     * @Route("/robots")
     * @Method("GET")
     */
    public function getAll()
    {
        $data = $this->getRepo()->findRobots();
        $result = parent::resultBuilder(parent::STATUS_OK, parent::STATUS_ZERO_RESULTS, $data);

        return parent::responseBuilder($result);
    }

    /**
     * @Route("/robots/{id}", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     */
    public function update(Request $request)
    {
        $jsonData = json_decode($request->getContent(), true);
        $id = $request->get('id');

        if($id) {
            $res = $this->getRepo()->findRobot($id);

            if(sizeof($res)==0) {
                $message = $this::MESSAGE_ENTRY_DOES_NOT_EXISTS;
                $result = parent::resultBuilder(parent::STATUS_UPDATED, parent::STATUS_ERROR, null, $message);

                return parent::responseBuilder($result, Response::HTTP_CONFLICT);
            }else {
                $obj = $this->getRepo()->find($id);
                $data['id'] = $id;
                $data['name'] = isset($jsonData['name'])?$jsonData['name']:$obj->getName();
                $data['type'] = isset($jsonData['type'])?$jsonData['type']:$obj->getType()->getType();
                $data['year'] = isset($jsonData['year'])?$jsonData['year']:$obj->getYear();


                $method = $request->getMethod();


                $response = $this->robotValidator($data, $method);
                $message = $response['message'];
                $status = $response['status']==null?Response::HTTP_OK:$response['status'];
                $canUpdate = !isset($message);


                if ($canUpdate == true) {
                    $type = $this->getDoctrine()->getRepository('AppBundle:Type')->findOneBy(array(
                        'type' => $data['type']
                    ));

                    $obj->setName($data['name']);
                    $obj->setType($type);
                    $obj->setYear($data['year']);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($obj);
                    $em->flush();

                    $data = $this->getRepo()->findRobot($obj->getId());
                }
                else{
                    $data = null;
                }

                $result = parent::resultBuilder(parent::STATUS_UPDATED, parent::STATUS_ERROR, $data, $message);
                return parent::responseBuilder($result,$status);
            }
        }else
            return parent::internalServerErrorResponse();
    }

    /**
     * @Route("/robots")
     * @Method("POST")
     */
    public function create(Request $request)
    {
        $jsonData = json_decode($request->getContent(), true);
        $method = $request->getMethod();

        $response = $this->robotValidator($jsonData, $method);
        $message = $response['message'];
        $status = $response['status']==null?Response::HTTP_CREATED:$response['status'];
        $canCreate = !isset($message);

        if ($canCreate === true) {
            $type = $this->getDoctrine()->getRepository('AppBundle:Type')->findOneBy(array(
                'type' => $jsonData['type']
            ));
            $obj = new Robot();
            $obj->setName($jsonData['name']);
            $obj->setYear($jsonData['year']);
            $obj->setType($type);

            $em = $this->getDoctrine()->getManager();
            $em->persist($obj);
            $em->flush();

            $data = $this->getRepo()->findRobotByName($obj->getName());
        }
        else {
            $data = null;
        }

        $result = parent::resultBuilder(parent::STATUS_CREATED, parent::STATUS_ERROR, $data, $message);
        return parent::responseBuilder($result, $status);
    }

    /**
     * @Route("/robots/{id}", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     */
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $res = $this->getRepo()->findOneBy(array(
            'id' => $id,
            "deleted" => false
        ));
        if(sizeof($res)>0){
            $obj = $this->getRepo()->find($id);
            $obj->setDeleted(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($obj);
            $em->flush();

            $message = null;
            $data = true;
            $response = Response::HTTP_OK;
        }else{
            $message = parent::MESSAGE_ENTRY_DOES_NOT_EXISTS;
            $data = false;
            $response = Response::HTTP_CONFLICT;
        }
        $result = parent::resultBuilder(parent::STATUS_DELETED, parent::STATUS_ERROR, $data, $message);
        return parent::responseBuilder($result,$response);
    }

    /**
     * @Route("/robots/search/{name}")
     * @Method("GET")
     */
    public function getByName($name){
        $data = $this->getRepo()->findRobotsByName($name);
        $result = parent::resultBuilder(parent::STATUS_FOUND, parent::STATUS_NOT_FOUND, $data);

        return parent::responseBuilder($result);
    }

    /**
     * @Route("/robots/filter/{typeName}")
     * @Method("GET")
     */
    public function getByType($typeName){
        $type = $this->getDoctrine()->getRepository('AppBundle:Type')->findBy(array(
            'type' => $typeName
        ));

        $statusElse = parent::STATUS_NOT_FOUND;
        $message = null;
        if(!$type) {
            $statusElse = parent::STATUS_ERROR;
            $message = parent::MESSAGE_INVALID_FILTER_VALUE;
        }

        $data = $this->getRepo()->findRobotsByType($typeName);
        $result = parent::resultBuilder(parent::STATUS_FOUND, $statusElse, $data, $message);

        return parent::responseBuilder($result);
    }

    public function robotValidator($jsonData, $method){
        if(isset($jsonData['name']) && isset($jsonData['year']) && isset($jsonData['type'])) {
            $type = $this->getDoctrine()->getRepository('AppBundle:Type')->findOneBy(array(
                'type' => $jsonData['type']
            ));

            $robotByName = $this->getRepo()->findOneBy(array(
                'name' => $jsonData['name']
            ));

            $message = null;
            $status = null;

            if (($robotByName && $method == 'POST')
                || ($robotByName && isset($jsonData['id']) && $method == 'PUT' && $jsonData['id'] != $robotByName->getId())) {
                $message = $this::MESSAGE_NAME_VALUE_MUST_BE_UNIQUE;
                $status = Response::HTTP_CONFLICT;
            }

            if (!$type) {
                $message = $this::MESSAGE_TYPE_VALUE_IS_INVALID;
                $status = Response::HTTP_CONFLICT;
            }

            if (strlen($jsonData['year']) != 4 || !is_numeric($jsonData['year'])) {
                $message = $this::MESSAGE_PARAMETER_YEAR_MUST_BE_4_DIGIT;
                $status = Response::HTTP_BAD_REQUEST;
            }

            return array(
                'message' => $message,
                'status' => $status
            );
        }else{
            return array(
                'message' => parent::MESSAGE_BAD_REQUEST,
                'status' => Response::HTTP_BAD_REQUEST
            );
        }
    }


}