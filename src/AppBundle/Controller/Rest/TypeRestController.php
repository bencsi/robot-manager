<?php

namespace AppBundle\Controller\Rest;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class TypeRestController extends AbstractRestController
{
    protected function getRepo()
    {
       return $this->getDoctrine()->getRepository('AppBundle:Type');
    }

    /**
     * @Route("/types/{id}")
     * @Method("GET")
     */
    public function getItem(Request $request)
    {
        return parent::notImplementedResponse();
    }

    /**
     * @Route("/robots/filter")
     * @Method("GET")
     */
    public function getAll()
    {
        $data = $this->getRepo()->findRobotTypes();
        $result = parent::resultBuilder(parent::STATUS_OK, parent::STATUS_ZERO_RESULTS, $data);

        return parent::responseBuilder($result);
    }

    /**
     * @Route("/types/{id}")
     * @Method("PUT")
     */
    public function update(Request $request)
    {
        return parent::notImplementedResponse();
    }

    /**
     * @Route("/types")
     * @Method("POST")
     */
    public function create(Request $request)
    {
        return parent::notImplementedResponse();
    }

    /**
     * @Route("/types/{id}")
     * @Method("DELETE")
     */
    public function delete(Request $request)
    {
        return parent::notImplementedResponse();
    }


}