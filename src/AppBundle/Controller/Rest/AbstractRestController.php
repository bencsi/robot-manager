<?php

namespace AppBundle\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


abstract class AbstractRestController extends Controller implements RestInterface
{
    const STATUS_CREATED = 'CREATED';
    const STATUS_DELETED = 'DELETED';
    const STATUS_ERROR = 'ERROR';
    const STATUS_FOUND = 'FOUND';
    const STATUS_NOT_FOUND = 'NOT-FOUND';
    const STATUS_OK = 'OK';
    const STATUS_UPDATED = 'UPDATED';
    const STATUS_ZERO_RESULTS = 'ZERO_RESULTS';

    const MESSAGE_BAD_REQUEST = 'Bad request';
    const MESSAGE_ENTRY_DOES_NOT_EXISTS = 'Entry does not exists';
    const MESSAGE_INTERNAL_SERVER_ERROR = 'Internal Server Error';
    const MESSAGE_INVALID_FILTER_VALUE = 'Invalid filter value';
    const MESSAGE_NOT_IMPLEMENTED = 'Not implemented';

    protected function getApiFormat(){
       return $this->getParameter('api_format');
    }

    protected function notImplementedResponse(){
        $result['status'] = $this::STATUS_ERROR;
        $result['message'] = $this::MESSAGE_NOT_IMPLEMENTED;
        return $this->responseBuilder($result,Response::HTTP_NOT_IMPLEMENTED);
    }

    protected function internalServerErrorResponse(){
        $result['status'] = $this::STATUS_ERROR;
        $result['message'] = $this::MESSAGE_INTERNAL_SERVER_ERROR;
        return $this->responseBuilder($result,Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    protected function resultBuilder($statusIf, $statusElse, $data=null, $message=null){
        if((is_array($data) && sizeof($data)>0) || $data == true) {
            $result['status'] = $statusIf;
            $result['data'] = is_array($data)?$data:null;
        }else{
            $result['status'] = $statusElse;
            $result['message'] = is_string($message)?$message:null;
        }

        return $result;
    }


    protected function responseBuilder($result, $status = Response::HTTP_OK){
        return new Response($this->get('jms_serializer')->serialize($result, $this->getApiFormat()), $status);
    }

    abstract protected function getRepo();

}