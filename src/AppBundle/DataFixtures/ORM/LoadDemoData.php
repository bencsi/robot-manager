<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Config\FileLocator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoadDemoData extends Controller implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->in('src/AppBundle/Resources/sql');
        $finder->name('demoData.sql');

        foreach( $finder as $file ){
            $content = $file->getContents();

            $stmt = $this->container->get('doctrine.orm.entity_manager')->getConnection()->prepare($content);
            $stmt->execute();
        }
    }
}