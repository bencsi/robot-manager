Robot Manager
==============

A Symfony2 based REST API to manage Robots.
This demo app also included a simple web app written in AngularJs.

How to install?
---------------------------


* `git clone https://bencsi@bitbucket.org/bencsi/robot-manager.git`
* `composer install`
* Edit `app/config/parameters.yml` and configure
     credentials to acces a database for this demo.
* `php app/console doctrine:database:create`
* `php app/console doctrine:schema:create`
* `php app/console doctrine:fixtures:load`
* `php app/console server:run`
* API url: `http://127.0.0.1:8000/api`
* Web app url: `http://127.0.0.1:8000`
 