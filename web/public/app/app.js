var app = angular.module("robotManager", [
    'ui.router'
])

.config(['$stateProvider','$urlRouterProvider',
    function ($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.otherwise('/overview');
    }
])
.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});