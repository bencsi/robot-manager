app.controller('RobotController', ['$scope','RobotFactory',
    function($scope,RobotFactory) {
        $scope.getRobots = function() {
            RobotFactory.get()
                .success(function (data) {
                    $scope.robots = data.data;
                });
        };
    }]);