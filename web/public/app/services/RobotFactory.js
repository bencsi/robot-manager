app.factory('RobotFactory', function($http) {
    return {
        get : function() {
            return $http.get(CONFIG.API_URL+'robots');
        }
    }
});